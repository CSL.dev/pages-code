---
layout: page
hide_footer: true
toc_title: Índice
title:  "4 Liberdades"
date:   2021-09-15 22:00:00
author: Eduardo Lopes Dias
categories: recursos
description: "Poster informativo sobre as 4 liberdades"
published: true
canonical_url: https://csl-rp.codeberg.page/recursos/2021/09/15/liberdades-poster.html
hero_link: false
hero_image: false
image: false
comments: false
hero_title: true
---

<center>
	<img 
		src="https://csl-rp.codeberg.page/assets/css/img/artigos/4liberdades/4liberdades.png"
		style="width: 100%"
	>
	<br>
</center>
<br> 

[Versão
SVG](https://csl-rp.codeberg.page/assets/css/img/artigos/4liberdades/4liberdades.svg).
Licença CC-BY-SA.

Traduzido por Guilherme Jardim.
