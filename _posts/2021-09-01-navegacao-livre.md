---
layout: page
hide_footer: true
menubar_toc: true
toc_title: Índice
title:  "Liberte sua navegação na Web"
date:   2021-09-01 15:37:00
author: Eduardo Lopes Dias
categories: ajuda
description: "Orientações sobre como proteger sua privacidade na
Web sem muita perda de conveniência"
published: true
canonical_url: https://csl-rp.codeberg.page/ajuda/2021/09/01/navegacao-livre.html
hero_link: false
hero_image: false
image: false
comments: false
hero_title: true
---

Queiramos nós ou não, atualmente boa parte de nossa computação pessoal envolve o uso de um navegador. Dessa forma, uma vez que se toma consciência do Software Livre, não demora muito a surgir o questionamento: Como minha navegação na rede pode ser mais livre, segura e privada?
Existe uma série de medidas que podem ser tomadas nesse sentido, muitas sem abdicar de nenhum tipo de conveniência. Esse artigo busca abordar alguns dos diferentes aspectos relativos a essa questão.

## Escolha de navegador

De forma mais que evidente, a escolha de um navegador desempenha papel de suma relevância em se tratando de navegação livre, segura e privada. [Principalmente quando o seu próprio navegador pode ser um spyware que, adicionalmente, vem com algumas ferramentas de navegação](https://www.mercurynews.com/2019/06/21/google-chrome-has-become-surveillance-software-its-time-to-switch/). Nossas recomendações de navegadores (e outros tipos de softwares também), assim como nossas justificativas para certas escolhas, podem ser acessadas [aqui](https://csl-rp.codeberg.page/ajuda/2021/04/29/lista-software-livre/#navegadores-web).

## Configurações do navegador

Embora imprescindivel, na grande maioria dos navegadores, a configuração padrão não provê tanta segurança quanto poderia, podendo ser aprimorada sem quaisquer perdas de conveniência ou utilidade, a depender das preferências do usuário. Nossas recomendações de configurações podem ser encontradas abaixo.

### Para o Firefox e derivados

A escrever...

### Para o Brave e outros navegadores baseados em Chromium

#### Shields

-   Em *Trackers & ads blocking*, escolha *aggressive*.

Isso bloqueará todos os trackers e anúncios em sites detectados que você acessar. Por experiência própria, essa configuração não acarreta em nenhuma perda de usabilidade do navegador ou funcionalidade de sites.

-   Em *Cookie blocking*, escolha *only cross-site*.

Isso bloqueará cookies que te seguem pela internet para traçar seu perfil de consumidor (O tipo de cookie ilustrado [aqui](https://www.mercurynews.com/2019/06/21/google-chrome-has-become-surveillance-software-its-time-to-switch/). Escolher *All* também é uma opção, mas pode acarretar em certa perda de conveniência.

-   Em *[Fingerprinting](https://www.mozilla.org/pt-BR/firefox/features/block-fingerprinting/) blocking*, escolha *strict*.

Isso bloqueará quaisquer fingerprintings detectados. É importante observar que, embora o navegador notifique que a decisão possivelmente deixará alguns sites quebrados, pessoalmente não experienciei nada do gênero. Caso isso ocorra e te cause incômodo significativo, escolha Standard.

#### Privacy and Security

-   Em *WebRTC IP handling policy*, escolha *Default public interface only*.

O WebRTC é uma ferramenta web que permite que você faça chamadas e videoconferências através do navegador. Como essa ferramenta se conecta diretamente com as pessoas que você está conversando, isso pode revelar seu endereço de IP à elas. A configuração escolhida inibe a exposição de quaisquer endereços locais. Novamente, não há perda de funcionalidade alguma em razão dessa escolha.

1.  *Security*

    -   Em *Safe Browsing*, escolha *No protection*.
    
    Embora não seja uma prática recomendada pelo navegador, o mecanismo usado para gerenciamento do Safe Browsing é uma API proprietária da Google, o que justifica a escolha. Essa configuração não deve causar quaisquer preocupações ao usuário, a não ser que tenha grande receio de clicar em links maliciosos. Nesse caso, mantenha a funcionalidade habilitada.

2.  *Site and Shields Settings*

    -   Em *Permissions*, clique em *Location* e desabilite.
    
    Isso impedirá que sites tenham acesso a sua localização geográfica. Não há, de forma geral, perda de funcionalidade, mas pode acarretar em certa perda de conveniência, como ter que inserir manualmente seu endereço em sites de entrega ou sites de localização geográfica.

#### Search Engine

-   Escolha um dos mecanismos de pesquisa presentes na nossa [lista de recomendações](https://csl-rp.codeberg.page/ajuda/2021/04/29/lista-software-livre/). Algumas das minhas recomendações pessoais são o Searx ou Brave Search, colocar um deles como padrão exigirá um pouco de trabalho adicional, mas nada muito complexo:
    
    -   Clique em *Manage search engines* e, em *Other search engines*, clique em *Add*.
    
    Para adicionar o Searx (particularmente a instância do disroot), preencha da seguinte forma:
    *Search Engine:* Searx Disroot
    *URL with %s in place of query:* <http://search.disroot.org/search?q=%s&categories=general&language=en-US>
    Para adicionar o Brave Search, preencha da seguinte forma:
    *Search Engine:* Brave
    *URL with %s in place of query:* <https://search.brave.com/search?q=%s&source=desktop>
    As *keyword*'s são arbitrárias, servem para facilitar a escolha do mecanismo de pesquisa que será utilizado para determinada pesquisa. Para fazer isso basta digitar a keyword na barra de pesquisa e dar *Tab*.
    Minhas keywords para o Searx e Brave são, respectivamente:
    *Keyword:* :sd
    *Keyword:* :br

#### Extensions

-   Desabilite o *Hangouts*.

Embora ele seja usado por padrão para compartilhamento de tela e afins, o mesmo é um software proprietário da Google, justificando a decisão. Desabilitá-lo acarreta na perda, obviamente, na funcionalidade de compartilhamento de tela em alguns softwares como Google Meet (O que pode ser contornado pelo uso de Jitsi, Big Blue Button, Jami e/ou outras ferramentas que não dependam do Hangouts para essa função).

-   Desabilite o *Private window with Tor*.

Caso deseje utilizar Tor, o melhor navegador para isso é o Tor Browser.

-   Desabilite o *Widevine*.

Widevine é um [DRM](https://drm-pt.info/o-que-e-drm/) exigido por plataformas como Netflix, Spotify e Amazon Prime para serem utilizadas. Novamente, essa é uma recomendação, veja como dever moral somente se identificar sentido na causa [Anti-DRM](https://www.defectivebydesign.org/).

#### Configurações adicionais

Algumas configurações adicionais bastante pertinentes podem ser feitas, para isso, cole no navegador: brave://flags/ (ou, para outros derivados do chromium, chrome://flags/)

-   Em *Isolate additional origins* escolha *Enabled*.
-   Em *Strict-Origin-Isolation* escolha *Enabled*.

Junto com a configuração anterior, esse item reforçará o isolamento de sites, dificultando *tracking*. Não há perda decorrente de funcionalidade.

-   Em *Freeze User-Agent request header* escolha *Enabled*.

Essa configuração reforça a dificuldade de que sites façam *fingerprinting*. Não há perda decorrente de funcionalidade.

## Extensões de navegador

Extensões são ferramentas de grande conveniência para personalização de diferentes softwares, podendo ser particularmente utilitárias para os propósitos abordados nesse artigo. As extensões de navegador mencionadas abaixo estão disponíveis para ambas as nossas principais recomendações de navegador web.

### Privacy Redirect

Essa extensão direciona mídias sociais proprietárias como Twitter, YouTube, Instagram e Reddit a sites com front-ends livres para os mesmos serviços, como Nitter, Invidious, Bibliogram e Libreddit, o que vem com os benefícios de não terem anúncios ou tracking, não exigirem que o Javascript esteja habilitado, além de serem mais leves que suas contrapartidas.
Além disso, o Privacy Redirect permite redirecionamento automático ferramentas da Google, como o Maps, o Tradutor e o próprio mecanismo de pesquisa à contrapartidas mais respeitosas ao usuário.
É importante observar que, embora muito conveniente, essa extensão não dá ao usuário liberdade plena. Uma vez que a back-end dos sites para os quais o usuário é redirecionado, por ser completamente inacessível em termos de código-fonte, continua proprietária.

### uBlock Origin

Essa extensão consiste de um *wide-spectrum blocker*, bloqueando não apenas anúncios, mas também *trackers* e **muitos** sites maliciosos presentes, os quais estão nas seguintes listas:

-   [Easy List](https://easylist.to/easylist/easylist.txt)
-   [Easy Privacy](https://easylist.to/easylist/easyprivacy.txt)
-   [Peter Lowe’s ad/tracking/malware servers](https://pgl.yoyo.org/adservers/policy.php)
-   [Online Malicious URL Blocklist](https://gitlab.com/curben/urlhaus-filter#urlhaus-malicious-url-blocklist)
-   [E outras listas próprias do uBlock](https://github.com/uBlockOrigin/uAssets/tree/master/filters)


### Privacy Badger

Juntamente com o uBlock, esta é a outra extensão essencial para
salvaguardar sua privacidade na web com efeitos colaterais
negligíveis. O [Privacy Badger](https://privacybadger.org) é um projeto da [Electronic Frontier
Foundation](https://www.eff.org/about), uma organização sem fins lucrativos que faz ativismo
em defesa dos direitos de usuários de computadores, em particular
a liberdade de expressão e direito à privacidade.

Sem entrar em detalhes de seu funcionamento, ela é
um conjunto de algoritmos que detecta e bloqueia automaticamente
os rastreadores mais perniciosos que acompanham quais sites você
visita pela Web.  Muitos desses rastreadores são obfuscados por
conteúdo no website, e são criados e atualizados a todo momento, o
que justifica a necessidade de um algoritmo para deduzir o que é
um rastreador novo com base em seu comportamento e bloqueá-lo.

### Cookie Autodelete

Cookies são pequenos arquivos de texto enviados ao seu computador pelo site que você está visitando. Se aceitos, eles são salvos em seu navegador e então podem fazer tracking e coletar dados do seu navegador, enviando essa informação de volta ao site.
Sendo recorrente que sites violem a liberdade 0 de seus usuários, os impedindo de fazer uso do site caso não aceitem cookies, não raro é que os aceitemos a contragosto, cedendo nossas informações para esses sites. Deletar esses cookies após realizar uso do site, por outro lado, muito embora seja o recomendado, não é uma prática comum. Essa extensão, como o nome pode sugerir, automatiza esse processo.
Essa extensão é altamente configurável, sendo possível mudar a
frequência e as ocasiões em que os cookies são deletados, e
desativar as notificações de deleção de cookies.

### I Don't Care About Cookies

Complementar a extensão anterior e muitíssimo conveniente, essa extensão bloqueia uma extensa lista de sites de exibirem notificações sobre cookies, libertando o usuário de ter que aceitá-los para simplesmente usar o site, ou mesmo ter que inspecionar elemento para remover a notificação.

Estes avisos foram implementados para que os sites pudessem
conformar com as diretrizes de privacidade digital da União
Européia. Vale notar que simplesmente ocultar estes avisos não
desabilitará os cookies, e esta extensão muitas vezes aceitará
todos os cookies do website por padrão. Mas se você seguiu nossas recomendações até aqui, esses cookies serão ou isolados ou apagados assim que você sair do navegador.

### CC Search

Essa extensão permite que se busque e encontre com facilidade na internet diferentes imagens licenciadas sob as licenças [Creative Commons](https://creativecommons.org/), permitindo que se filtre as imagens exibidas de acordo com a licença escolhida. Nenhuma das imagens tem marca d'água e a extensão não exige nenhum tipo de login para ser utilizada.
Pode ser utilitária uma descrição breve das diferentes licenças CC, o que pode ser encontrado [aqui](https://creativecommons.org/licenses/).
Embora essa extensão não entre exatamente no mérito de segurança e privacidade, ela se mostra significativamente utilitária no propósito de ter uma navegação mais livre, o que justifica sua presença nesse artigo.

## Mecanismo de pesquisa

O papel da escolha de um mecanismo de pesquisa adequado é significativo. Como é sabido de longa data, mecanismos de pesquisa como Google regularmente fazem coletas de dados de seus usuários, que além de serem vendidos, podendo ser comprados por empresas que disseminam notícias falsas para maximizarem sua disseminação (como aconteceu nas eleições americanas de 2016, ou nas brasileiras de 2018), servem para personalizar resultados de pesquisa, introduzindo novos vieses na opinião pública. Nossas recomendações de mecanismos de pesquisa podem ser encontradas [aqui](https://csl-rp.codeberg.page/ajuda/2021/04/29/lista-software-livre/#mecanismos-de-busca).

## Sites

Outra ferramenta efetiva para ter uma navegação mais livre, convenientemente bastante simples, é fazer uma seleção dos sites que se acessa. Dessa forma, ao navegar na internet, **evite sites maliciosos** sempre que possível. Isso pode ser feito de diferentes formas, algumas das quais são apresentadas abaixo.

### Lembre-se: Onde cabe um, cabem dois

Sites nos quais seu navegador e/ou suas extensões indicam grande presença trackers, fingerprinting e afins devem ser evitados, uma vez que, tendo comprovações da determinação desses sites em te espionar, não seria improvável que eles tenham em si outros malwares que ainda não são detectados pelas ferramentas que você utiliza.

### Opte por sites livres

Sendo o caso que [software proprietário é, frequentemente, malware](https://www.gnu.org/proprietary/proprietary.en.html), sem exceção para sites, optar por sites livres nos seguintes casos se mostra uma boa forma de evitar sites maliciosos:

-   Se precisar serviços de edição de PDF's, conversão de arquivos e afins;
-   Se procurar por conhecimento, cultura, software, hardware, ou recursos artísticos;
-   Sempre que possível.

### Use menos o navegador

Consideravelmente efetivo, fazer um uso menos regular do navegador traz benefícios diversos, principalmente a segurança. Alguns dos outros benefícios dessa decisão são:

#### Ter uma computação mais modular

Modularidade consiste de ter diferentes softwares para diferentes trabalhos. Sofwares com funcionalidades específicas para determinado propósito, naturalmente, tendem a ser mais eficientes do que um software que realiza trabalhos notoriamente diferentes, dado que cada utilidade do software demanda mais linhas de código, e é muito mais fácil maximizar a eficiência de um código de 1000 linhas do que de um código de 10000.

#### Focar no que precisa ser feito

Sendo a internet grandiosamente interconectada, ela se mostra uma ferramenta de grande utilidade para obtenção de conhecimento, o que é contente. Por outro lado, a simples possibilidade de ir, em alguns instantes, de um única aba aberta (na qual se precisa focar) para um número arbitrário de abas não parece uma boa forma de maximizar o foco.

#### Ter mais tempo livre

Consequência direta do item anterior.

## Mídias Sociais

Uma das principais razões pelo quais as pessoas acessam a internet, as mídias sociais, volta e meia chamadas erroneamente de redes sociais, permitem interagir com diferentes pessoas ao redor do mundo e são cada vez mais integradas na vida das pessoas, em empresas, instituições de ensino e afins, podendo ser consideradas praticamente essenciais para viver em sociedade.
Infelizmente, as redes sociais conhecidas pelo usuário médio são, em sua absoluta maioria, proprietárias e, não raro, envolvidas em diversos [escândalos](https://www.vox.com/recode/2019/10/31/20940532/edward-snowden-facebook-nsa-whistleblower), desde algoritmos capazes de traçar perfis extremamente detalhados do usuário, vazamento de dados de usuários, monopólios e entrega de dados a agências de segurança estadiunidenses, ao uso de algoritmos para induzir sentimentos e ideias em seus usuários, e a lista não para aqui. Nesse sentido, procurar por midias sociais que respeitem o usuário é algo urgente, assim como a transição para essas outras plataformas.
Nossas recomendações de mídias sociais livres podem ser encontradas [aqui](https://csl-rp.codeberg.page/ajuda/2021/04/29/lista-software-livre/#midias-sociais).

## Gerenciador de senhas

Ainda que usemos navegadores extremamente seguros, é notório que esse tipo de software é o principal alvo de [malwares](https://wikiless.org/wiki/Malware?lang=pt#Principais_tipos) dos mais diferentes tipos. Fazendo com que guardar neles logins, senhas de sites, cartões de banco e afins não seja exatamente uma boa prática. Por outro lado, é grande o inconveniente de ter que lembrar diferentes senhas, principalmente quando são, como é recomendado, longas e complexas. Uma solução simples e eficaz para isso é fazer uso de um gerenciador de senhas, ou *password manager*, em inglês. Como o próprio nome já diz, esse tipo de software permite gerenciar suas senhas, que ficam guardadas no seu computador, em um arquivo que serve de banco de dados, sendo o conteúdo desse arquivo acessível somente com uma senha (Essa você precisa lembrar).
Diversos gerenciadores de senhas vêm, ainda, com funcionalidades bastante convenientes, como:

-   Geradores de senhas aleatórias, de complexidade e tamanho escolhidos pelo usuário;
-   *Autotyping;*

Recurso que permite que logins, senhas, palavras chave e afins, sejam de sites ou de serviços, sejam preenchidos com um simples atalho de teclado, escolhido pelo usuário. Nossas recomendações de gerenciadores de senhas podem ser encontradas [aqui](https://csl-rp.codeberg.page/ajuda/2021/04/29/lista-software-livre/#gerenciadores-de-senhas).

