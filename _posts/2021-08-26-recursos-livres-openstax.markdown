---
layout: page
show_sidebar: true
hide_footer: true
title:  "Conteúdo Livre - OpenStax"
date:   2021-08-26 15:55:00
author: Eduardo Lopes Dias
categories: recursos
description: "Neste artigo apresentamos os livros-texto de nível
médio e universitário produzidos e distribuídos livremente pela
OpenStax."
published: true
canonical_url: https://csl-rp.codeberg.page/filosofia/2021/08/26/openstax.html
hero_link: false
hero_image: false
image: false
comments: false
hero_title: true
---

# Série Conteúdo Livre

Este é o primeiro artigo da série **Conteúdo Livre**, onde
apresentaremos uma variedade de recursos educacionais, culturais,
científicos e etc distribuídos sob licenças livres, e que
julgamos merecedores de mais atenção no nosso meio universitário.

Inscreva-se em nosso [feed
RSS](https://csl-rp.codeberg.page/feed.xml) ou entre em [nosso
grupo do Telegram](https://csl-rp.codeberg.page/contato/) para ficar por
dentro dos nossos próximos artigos.

# O atual estado das coisas

Qualquer estudante universitário brasileiro pode atestar que o
livro universitário é um recurso em perpétua escassez. O custo
exorbitante destes materiais, a quantidade crescente de alunos, a
necessidade de constante atualização para novas edições, e a
grande variedade de livros que o aluno precisa ler ao longo de
sua formação faz com que a aquisição de quantidades ideais de cópias
exija um orçamento infinito para as bibliotecas universitárias.

Graças aos [direitos
autorais](https://odysee.com/@CSL-RP:6/direitos-autorais-dano-cerebral:6),
o acesso aos livros por meios digitais também apresenta
empecilhos. As bibliotecas digitais das universidades, além de
possuírem acervos muito inferiores aos de suas contrapartidas
físicas, têm de adotar medidas para conter o compartilhamento de
cópias não autorizadas, fazendo o acesso ao conteúdo digital
tamanha inconveniência que a luta por cópias físicas é ainda
preferível.

Tudo isso leva os estudantes a contrabandear cópias digitais em
segredo aberto entre si, o que torna a
["pirataria"](https://www.gnu.org/philosophy/words-to-avoid.pt-br.html#Piracy)
a maneira mais acessível e conveniente de dar continuidade à sua
educação.

Em reconhecimento a essa bizarra (e cômica) realidade, somos
levados a nos perguntar por quê a adoção de conteúdo didático
aberto não é a norma em cursos universitários. Felizmente, existem
iniciativas para a produção de livros-texto de alta qualidade e
livremente acessíveis. Uma tal iniciativa particularmente bem
sucedida é a **OpenStax**.

# A OpenStax

A [OpenStax](https://openstax.org/about) é uma organização sem
fins lucrativos que faz parte da Universidade Rice nos Estados
Unidos. Seu objetivo é a transformar a educação através da
acessibilidade, o que é feito através da produção de livros-texto
e materiais complementares **disponibilizados gratuitamente e sob
licenças abertas**.

O [website da
organização](https://openstax.org/subjects/view-all) conta
atualmente com 42 livros para ensino superior e médio, que
abrangem as disciplinas iniciais de cursos de todas as áreas do
conhecimento. Os livros são escritos e revisados por professores
com experiência em ensino na área, e contam com exercícios,
resoluções, vídeos e simulações interativas (para fenômenos físicos e
conceitos matemáticos, por exemplo).

<br>
<center><img
 	src="https://csl-rp.codeberg.page/assets/css/img/artigos/openstax/bitmap.png"
	style="width: 90%">
</center> 
<center>
	<i>Alguns dos livros-texto criados pela iniciativa OpenStax.</i>
</center>
<br> 

Para acessar os livros, basta visitar o website e fazer o
download do PDF, ou ler no próprio site. Também é possível
comprar cópias físicas (o que infelizmente é financeiramente
inacessível para brasileiros).

O conteúdo e abrangência do material da OpenStax não perde para os
livros-texto proprietários comumente utilizados nas disciplinas
básicas de cada curso. Mas sem dúvidas o melhor aspecto desse
conteúdo é a liberalidade com a qual ele pode ser distribuído e
modificado: a licença
[CC-BY](https://en.wikipedia.org/wiki/Creative_Commons_license)
(sob a qual é distribuído) te garante total liberdade sobre o
conteúdo, contanto que você atribua os autores em suas
redistribuições e variações.

<br>
<center><img
 	src="https://csl-rp.codeberg.page/assets/css/img/artigos/openstax/screenshot.png"
	style="width: 40vw">
</center> 
<center>
	<i>Screenshot do University Physics 3, visto pelo website.
	Dica aos alunos: vocês podem usar as figuras destes livros em
	seus relatórios! contanto que atribuam o livro, claro. </i>
</center>
<br> 

Infelizmente, os livros-texto da OpenStax possuem um empecilho
óbvio à sua missão de acessibilidade: a grande maioria só está
disponível em inglês (o University Physics está em polonês
também). Fato é que atualmente muitos professores já adotam
livros que só estão disponíveis em inglês, mas isso ainda assim é
uma grande desvantagem para o OpenStax.

# Um apelo às instituições de ensino superior

O material da OpenStax já está sendo amplamente adotado nos EUA e
gradualmente em outros países anglofones, mas sua missão de
"transformar a educação" é limitada por sua disponibilidade em
somente uma língua. A adoção destes livros já gerou economias
milionárias nos Estados Unidos, mas os países que mais
beneficiariam dessa acessibilidade e economia são os em
desenvolvimento, como o Brasil.

Temos certeza que o cenário descrito na introdução deste artigo é
familiar a todo estudante, e não é novidade a nenhum professor.
No entanto, este é o *status quo* há tanto tempo que hoje temos
isso como normal, assim como acontece com o uso de software
proprietário nas universidades. Precisamos nos dar conta
novamente de que essa situação é aberrante, e uma maneira de
atacá-la é com a criação e difusão de livros-texto livres
como os da OpenStax.

Fica, portanto, o nosso apelo e sugestão às instituições de
ensino superior, especialmente as públicas: **adotem, traduzam e
adaptem o OpenStax à realidade brasileira**. Projetos de tradução
e adaptação deste material trariam um grande benefício à
comunidade acadêmica, na medida que disponibilizaria recursos de alta
qualidade acessíveis por **todos** os estudantes, e tiraria das
bibliotecas um pouco do ônus de comprar e manter um grande número
de livros didáticos para disciplinas básicas. Afinal, a permissão
de redistribuir e modificar nós já temos.
